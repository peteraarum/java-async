package se.cygni.async.ex2;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeoutException;

public class WaitNotify {


    /**
     * Executes the callable in a separate thread waiting for timeout milliseconds for the call to complete.
     *
     * @param callable
     * @param timeout the maximum number of milliseconds to wait for a result.
     * @param <V>
     * @return the result from <code>callable.call()</code>
     * @throws InterruptedException if this thread was interrupted waiting for a result.
     * @throws TimeoutException if timeout expired.
     */
    public static <V> V executeAsync(Callable<V> callable, int timeout) throws InterruptedException, TimeoutException {
        return null;
    }

}
