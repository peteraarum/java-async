package se.cygni.async.ex3;

public interface BankAccount {
    int getTotal();

    void deposit(int amount);
}
