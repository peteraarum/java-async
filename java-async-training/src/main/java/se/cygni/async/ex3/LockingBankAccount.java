package se.cygni.async.ex3;

/** Make this class thread safe using {@link java.util.concurrent.locks.ReentrantLock} */
public class LockingBankAccount implements BankAccount {
    private int total;

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public void deposit(int amount) {
        total += amount;
    }
}
