package se.cygni.async.ex1;

public class CachingPrimeService extends PrimeService {

    /**
     * Before trying to compute if number is a prime we check:
     * <ol>
     * <li> If number is in prime cache, then return true.</li>
     * <li> If number is divisible by any prime in the prime cache, then return false.</li>
     * <li> Otherwise call {@link super#isPrime(long)}, cache the result if true and return.</li>
     * </ol>
     *
     * Think about thread safety!
     * @param number
     * @return
     */
    @Override
    public boolean isPrime(long number) {
        throw new RuntimeException("TODO");
    }
}
