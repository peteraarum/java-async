package se.cygni.async.ex4;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import se.cygni.async.Exercise;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Supplier;

import static org.slf4j.LoggerFactory.getLogger;

@RestController
public class EmailController {

    private static final Logger LOGGER = getLogger(EmailController.class);

    @Autowired
    private UserDao userDao;

    /**
     * Use {@link CompletableFuture#supplyAsync(Supplier)} and {@link CompletableFuture#thenAccept(Consumer)} to
     * do a fire and forget lookup and sending of the email.
     */
    @Exercise
    @RequestMapping(path = "/email/{name}", method = RequestMethod.POST)
    public String emailUser(@PathVariable String name) {
        Optional<User> user = userDao.findUser(name);
        user.ifPresent(this::emailUser);
        return "Email will be sent.";
    }

    private void emailUser(User user) {
        LOGGER.info("Emailing " + user.getName());
    }
}
