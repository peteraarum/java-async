package se.cygni.async.ex1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import se.cygni.async.Exercise;

public class CachingPrimeServiceTest {

    private CachingPrimeService cachingPrimeService;

    @Before
    public void before() throws Exception {
        cachingPrimeService = new CachingPrimeService();

    }

    @Test
    public void testIsPrime() throws Exception {
        PrimeService primeService = new PrimeService();
        for (int i = 0; i < 10000; i++) {
            Assert.assertEquals(primeService.isPrime(i), cachingPrimeService.isPrime(i));
        }
    }

    /**
     * Compare the running time of the caching and non-caching prime services.
     */
    @Exercise
    @Test
    public void testComparePerformance() throws Exception {
    }
}