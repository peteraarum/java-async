package se.cygni.async.ex1;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.cygni.async.Exercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

public class PrimeServiceTest {

    private static Logger LOGGER = LoggerFactory.getLogger(PrimeServiceTest.class);

    public static final List<Integer> PRIMES = Arrays.asList(
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29
            , 31, 37, 41, 43, 47, 53, 59, 61, 67, 71
            , 73, 79, 83, 89, 97, 101, 103, 107, 109, 113
            , 127, 131, 137, 139, 149, 151, 157, 163, 167, 173);
    private PrimeService primeService;

    @Before
    public void before() throws Exception {
        primeService = new PrimeService();
    }

    @Test
    public void testIsPrime() throws Exception {
        List<Integer> actual = new ArrayList<>();
        Integer last = PRIMES.get(PRIMES.size() - 1);
        for (int i = 0; i <= last; i++) {
            if (primeService.isPrime(i)) {
                actual.add(i);
            }
        }
        assertEquals(PRIMES, actual);
    }

    @Test
    public void testFirst() throws Exception {
        assertEquals(157, primeService.first(152));
    }

    /**
     * Compute 5 primes in separate threads. Each thread logs their result to LOGGER.
     */
    @Exercise
    @Test
    public void testLogPrimeInThreads() throws Exception {
        // TODO
        LOGGER.info("Compute prime in own thread.");
    }

    /**
     * Compute 5 primes in separate threads. Each thread adds its result to a list.
     * Wait for them to finish and print the results to LOGGER.
     * <p>
     * Use {@link java.util.Collections#synchronizedList(List)}
     * to avoid having to synchronize on the list yourself.
     */
    @Exercise
    @Test
    public void testComputePrimeInThreads() throws Exception {
        // TODO
    }

    /**
     * Write a version of {@link #testComputePrimeInThreads()} that uses a plain array list.
     * You need to use a synchronized-block.
     */
    @Exercise
    @Test
    public void testComputePrimeInThreads2() throws Exception {
        // TODO
    }

    /**
     * Use a {@link ExecutorService#submit(Callable)} to compute 1000 primes.
     * Wait for the result and compute the sum. Shutdown the executor and wait for it to terminate.
     * <p>
     * Play with different pool sizes and pool types. Do some logging in the task to see the what threads
     * that are active.
     */
    @Exercise
    @Test
    public void testComputePrimeInExecutor() throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(2);

    }

    /**
     * What happens when an exception is thrown in a thread or ExecutorService? Is there a difference between
     * {@link ExecutorService#execute(Runnable)}, {@link ExecutorService#submit(Runnable)}
     * and {@link ExecutorService#submit(Callable)} in how execptions are treated?
     */
    @Exercise
    @Test
    public void testExceptionsInThreads() throws Exception {
    }
}