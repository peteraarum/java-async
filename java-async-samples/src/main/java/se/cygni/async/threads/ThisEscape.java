package se.cygni.async.threads;

import java.awt.*;
import java.util.Arrays;
import java.util.EventListener;
import java.util.Random;
import java.util.stream.IntStream;

@SuppressWarnings("ALL")
public class ThisEscape {
    public ThisEscape(EventSource source) {
        source.registerListener(
                new EventListener() {
                    public void onEvent(Event e) {
                        doSomething(e);
                    }
                });
    }

    private void doSomething(Event e) {}
}


@SuppressWarnings("ALL")
class SafeListener {
    private final EventListener listener;
    private SafeListener() {
        listener = new EventListener() {
            public void onEvent(Event e) {
                doSomething(e);
            }
        };
    }
    public static SafeListener newInstance(EventSource source) {
        SafeListener safe = new SafeListener();
        source.registerListener(safe.listener);
        return safe;
    }

    private void doSomething(Event e) {}
}

class EventSource {
    public void registerListener(EventListener eventListener) {

    }
}


class SummingInts extends Thread {
    public SummingInts() {
        start();
    }

    public int work(int a, int b) {
        return a + b;
    }

    @Override
    public void run() {
        int result = IntStream.range(1, 11).reduce(0, this::work);
        System.out.println(result);
    }

    public static void main(String[] args) {
        new SummingInts();
    }
}

class FactoredSummingInts extends SummingInts {
    private final int factor;

    public FactoredSummingInts(int factor) {
        this.factor = factor;
    }

    public int work(int a, int b) {
        return factor*(a + b);
    }

    public static void main(String[] args) {
        new FactoredSummingInts(2);
    }
}