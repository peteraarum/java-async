package se.cygni.async.threads;

import java.util.Random;

public class SumIntsWait {
    private static Object LOCK = new Object();
    private static boolean done;
    private static long total;

    private static class SumThread extends Thread {
        public void run() {
            Random random = new Random();
            long total = random.ints(100000000).sum();
            System.out.println("sum() finished: " + total);

            synchronized (LOCK) {
                SumIntsWait.total = total;
                SumIntsWait.done = true;
                LOCK.notifyAll();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new SumThread().start();
        long total;
        synchronized (LOCK) {
            while (!done) {
                System.out.println("Waiting...");
                LOCK.wait();
            }
            total = SumIntsWait.total;
        }

        System.out.println("Got total: " + total);
    }
}
