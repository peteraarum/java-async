package se.cygni.async.threads;

import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.partitioningBy;
import static org.slf4j.LoggerFactory.getLogger;

@SuppressWarnings("Duplicates")
public class WaitNotifyThread {
    private static final Logger LOGGER = getLogger(WaitNotifyThread.class);
    public static final Random RANDOM = new Random();

    public static void main(String[] args) throws InterruptedException {
        Object signal = new Object();
        Runnable work = () -> {
            try {
                LOGGER.info("Starting work...");
                doWork();
                LOGGER.info("done.");
            } finally {
                synchronized (signal) {
                    signal.notify();
                }
            }
        };
        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Thread t = new Thread(work);
            LOGGER.info("Starting " + t.getName());
            t.start();
            threads.add(t);
        }

        LOGGER.info("Waiting for threads to finish...");
        synchronized (signal) {
            while (true) {
                Map<Boolean, List<Thread>> map = threads.stream().collect(partitioningBy(Thread::isAlive));
                map.getOrDefault(false, emptyList())
                        .forEach(t -> LOGGER.info(t.getName() + " finished."));
                threads = map.getOrDefault(true, emptyList());
                if (threads.isEmpty()) {
                    break;
                }
                signal.wait();
            }
        }
        LOGGER.info("All threads are finished.");
    }

    private static void doWork() {
        try {
            Thread.sleep(RANDOM.nextInt(2000) + 1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }


}
