package se.cygni.async.threads;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class TextAreaBackgroundUpdateExample extends Application {

    @Override
    public void start(Stage primaryStage) {
        final BorderPane root = new BorderPane();
        final TextArea textArea = new TextArea();
        final ProgressBar progress = new ProgressBar();
        final Button startButton = new Button("Start");

        final int maxCount = 500;

        startButton.setOnAction(event -> {
            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    for (int i = 1; i <= maxCount; i++) {
                        Thread.sleep(10);
                        final int count = i ;
                        // Updates of UI in UI-thread!
                        Platform.runLater(() -> textArea.appendText("Processed part " + count + " (of "+maxCount+")\n"));
                        updateProgress(i, maxCount);
                    }
                    return null;
                }
            };
            progress.progressProperty().bind(task.progressProperty());
            Thread t = new Thread(task);
            t.setDaemon(true);
            t.start();
        });

        root.setCenter(textArea);
        root.setTop(progress);
        root.setBottom(startButton);

        final Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}