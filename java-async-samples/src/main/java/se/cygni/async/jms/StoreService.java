package se.cygni.async.jms;

import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class StoreService {
    private final Map<String, Order> receivedOrders = new ConcurrentHashMap<>();

    public void registerOrder(Order order) {
        this.receivedOrders.put(order.getId(), order);
    }

    public Optional<Order> getReceivedOrder(String id) {
        return Optional.ofNullable(receivedOrders.get(id));
    }
}