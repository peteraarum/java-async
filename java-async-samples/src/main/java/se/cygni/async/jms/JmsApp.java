package se.cygni.async.jms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JmsApp {

    public static void main(String[] args) {
        SpringApplication.run(JmsApp.class, args);
    }
}